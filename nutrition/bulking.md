# Bulking

# Introduction

The essence of gaining weight is simple; eat more than you burn. Everybody burns a certain amount of calories just being alive and in order to bulk you need to consume more calories than this value.

Your Total Daily Energy Expenditure (TDEE) is a measure of how many calories you burn per day and can be calculated using an [online calculator](https://tdeecalculator.net/). Once you have this value the formula for gaining weight becomes simple;

*Bulking*

Total Calories Eaten - TDEE > 0

Of course the amount of calories you eat will determine how quickly you gain weight, nobody wants to get fat so there are ways to minimise fat gain and maximise muscle gains. A general rule of thumb is that you should be aiming for putting on 1kg per month which eqautes to around 0.5lb per week.

