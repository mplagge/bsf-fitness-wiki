# Cutting

> Cutting refers to a begin in a caloric deficit so you can lose fat.

For bulking see [bulking](doesntwork)

## How do I lose fat? 
To lose fat go into a calorie deficit, your body will seek energy from glycogen stores, and fat stores in your body and start burning them to get the energy they need, thus you will lose fat in the process. Cardio can help improve fat burning efficiency and mobilisation of fat, as well as attributing to a calorie deficit since cardio burns energy. Tracking your food accurately to cut fat is important, use food scales and use myfitnesspal to track your foods. Everything that goes inside of you needs to be tracked.

## Fat spot reduction

You can't lose fat in one spot. You have to lose overall body fat.

## Don't lose muscle

To preserve muscle you should decrease deficit slowly, however you should also keep protein high (AT-LEAST 1g/lbs of bodyweight). If you're in a very steep deficit with high physical activity you'll need even more protein to ensure your recovery and muscle breakdown aren't hindred.

## How do you know if you are in a caloric deficit?

TODO, but this calculator might help:
https://www.calculator.net/calorie-calculator.html
