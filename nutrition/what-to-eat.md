# Abstracted tip to improve your diet directly.

These tips might already improve your current diet, but for more elaboration and more specific information be sure to read the rest of the page.
* Read the [beginner guide](../beginner/beginner-basics.md) on if you should be cutting and bulking.
* Split up your big meals into smaller meals to eat more frequently throughout the day.
* Get a lot of protein in.
* Don't neglect Vegies! Eccasales ones are for example bloccoli, carrots, bell peppers, peas.
* Drink a lot of water
* A rule of thumb is to eat at least protein and a carb for a meal. For example chicken with peas.

# What is the most important thing for nutrition?

### 1. Calories

Make sure your caloric intake is correct first. Read the [beginner guide](../beginner/beginner-basics.md) on chosing either a cut of a bulk.

### 2. Macro's

Your macronutriens are protein fats and carbohydrates.

This should do the trick untill he write more info ourselfs:
https://www.bodybuilding.com/fun/macronutrients_calculator.htm

### 3. Micro's

Some foods have more viramins and minirals than other food. Whole eggs for example have more viramins than only the egg whites. It is important not to run into deficienties to optimise your hormines and help grow your body.

The foods you will be chosing should be as high in macronutriens as possible.

### Also very important

HYDRATION! Drink a lot of water. A simple rule of thumb is to drink when you are thirsty. If you pee is white you are drinking too much. If it has a bright yellow color you are probably drinking the right amount.

# How many meals should I have every day?

Usually more than four meals a day should be a great choice if you plan to build muscle. To make sure your body has the opportunity to grow througout the day. Some bodybuildilders eat up to 10 times a day.

# When should I eat my fats, carbs and protein?

You can optimise your workout by eating smart. Read [this](../workout_nutrition.md) article for more info.

TODO before bed, in the morning. tips on that...

# Supplementation

There are a lot of usefull supplements for bodybuilding. Do note that these are a sublimentation for real food. Be sure to get as much quallity nutrients from normal foods first.
Because you will be asking a lot from your body supplementation is good for convinience and insurance that you will get an optimal amount of nutrients in your body.
If you are fairly new to sublimentation I'dd recommend learning about what is in real foods first.

Some populair supplements to look into are:
* Whey protein isolate
* [Creatine](../nutrition/creatine_supplementation.md)
* Coffee
* Multivitamins
* Omega 3 capsules.

# What food should I eat?

Your caloric budget should be spend to get the following micronutients. Calculate your macro's and micro's and create some meals.

# Protein

High quallity Protein Sources:
* 90% Lean ground beef
* Chicken breast
* Other lean cuts of red and white meat.
* Whey Isolate
* Eggs or egg whites
* white and fat Fish
* Soft cheeses like cottage cheese, greek yoghurt
* Fat free milk (or lactose free for and even better protein ratio)

# Fats

* High omega 3 fish (salmon, sardines, mackerel, cod liver).
* Nuts (Brazil nuts, almonds, cashews, walnuts)- omega 6 primarily
* Nut butters (Peanut butter, almond butter)- omega 6 primarily 
* Seeds (flaxseed, chaiseed) omega 3 and omega 6
* Oils such (Olive oil and coconut oil)
* Grassfed butter 

# Carbs

* Granola
* White/brown rice
* White/brown pasta
* Wholegrain bread
* Berries, high in antioxidants and vitamins/minerals
* Veggies, high in antioxidants and vitamins/minerals
* Fruits 

# Fiber

TODO

# Vitamins and minerals

* Eat lots of fruit/vegetables
* Fish 
* Grassfed dairy products
* Eggs
* Red meat
* Seafood
* Vitamin supplementation, especially vitamin D and Omega 3

# Videos on low calorie and high calorie dense foods

* https://www.youtube.com/watch?v=iXD3c3qDE5s
* [Ethan Chlebowski lower calorie series](https://www.youtube.com/results?search_query=+Ethan+Chlebowski+lower+calorie+series) Professional chef creates accessible meals. They taste amazing and are diverse.

# This is all too difficult, create a meal plan for meal

* https://www.custommealplanner.com/ Good accurate meal planner, food is cheap and tastes bland but decent.