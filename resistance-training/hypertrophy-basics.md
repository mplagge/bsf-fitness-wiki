# Hypertrophy basics

* [Hypertrophy made simple](https://www.youtube.com/watch?v=4cW0EmO12Lk&list=PLyqKj7LwU2RukxJbBHi9BtEuYYKm9UqQQ&index=1)

# Tips on Hypertrophy
* Train hard, give it your all don't take it easy.
* Listen to your body, this is an extremely powerful tool your body will tell you everything. 
* If you're far too tired to train maybe think about skipping that day as your intensity will be down.
* Keep consistent without consistency you won't grow (this is most fundamental)
* Progressive overload your lifts, increase volume out put. (sets, reps and weight). Personally I would recommend increasing weight and reps primarily on each of your exercises.
* Variation of training. Do top sets and back off sets. I.e set 1 heavy, set 2 medium to light, set 3 medium to light. Progressively overload the top sets and back off sets week in week out. You will grow!
* Eat to fuel your growth, if you're hungry eat, if you're not hungry wait for your body to tell you to eat. 
* Have 4+ servings of 20-30g protein a day this will optimise muscle building
* Consider pre, intra and post workout nutrition. Eat lots of fast digesting carbohydrates pre and post workout to spike insulin levels for training, and replenish glycogen stores post workout. 
* Try not to eat too much high fibre/high food around your workout window as this will delay digestion of faster digesting carbs for optimal performance in the gym. 

