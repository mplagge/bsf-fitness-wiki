# Buying equipment

Buying equipment is becoming increasingly popular due to COVID-19, and gyms shutting down. Prices have skyrocketed, which sucks if you want to get started at home. But there are a lot of websites still selling equipment at normal prices luckely.

## What equipment should I buy (Small budget €100) 

With a small budget you will be looking to buy maybe a pair of adjustable dumbbells and a pull up bar.

|equipment|Price in € |description
|---      |---  |---|
| Pull up bar | 20 to 40 | Find a pull up bar that can be attached in your house on a door frame or drilled into the wall, make sure it is sturdy looking and with good reviews. Pull ups should be a staple movement for your back development, but the bar allows for a lot more excersices.
|Adjustable dumbbells | 50 to 100 | Get at least two 10 kg dumbells. It is recommended to get even more like 15kg if you find a good deal. This weight can get you started with a good routine. Buying cast iron can be a bit more expensive than sand/concrete, but usually the quallity is a bit better. If money is an issue buy the sand/concrete onces. These dumbells can help with isolation excersices, but can also help with weighing compounds like push ups, pull ups and lunges.

## What equipment should I buy? (big budget)

How much you will spend depends on your budget, and how sure you are about your fitness journey.  If you have a big budget, and know you really want a home gym and think its worth the intvestment. Its a good idea to start out on buying an olympic barbell, rack, bench, and olympic plates. A pull up bar and dumbells are also a must for a complete home gym. You should buy these first.

|equipment|Price in € |description
|---      |---  |---|
|Barbell | TODO | Look a 7ft olympic 20kg 2"(50mm) diamneter bar. |
|Rack | TODO | Buy a rack that can handle a good amount of weight, and that looks sturdy with good reviews. You can check the item description for how much weight it can handle |
| Plates | TODO | Get 2" (50mm) diameter plates to fit the bar. Get a good range of plates for your ability. 2.5kg, 5kg, 10kg, 20kg plates. |
| Bench | TODO | Buy a bench that can handle enough weight (again check the description of the item) Get one that can be adjusted up to an incline with a sturdy look to it. 

## Where should I buy equipment? 

* Ebay tends to be a good place for pretty cheap prices on gym equipment. You can fidn some second hand gym equipment, which will be perfectly okay, and win a bid cheaply (possibly). 

* Facebook marketplace can be a good place for finding gym second hand gym equipment some people might be selling equipment cheap in your area. 

* Amazon

* Argos (if you're from England) Do extremely cheap gym equipment although they can be out of stock in your area quite often, but its worth checking. 