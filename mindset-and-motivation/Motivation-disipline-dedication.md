# Motivation, disipline and dedication

Lets be blunt, if you need motivation to workout it is not sustainable. If you need to force yourself to do a workout, stick to a diet or do any fitness other related you can't sustain it for a very long time. In the best case it will take you a lot of energy while seeing results.

Is is very important that you actually like the workouts that you are doing, like the food you are eating etc...
Preferably you want to be dedicated about fitness, so that you actually want to sustain it for the rest of your life.

Maybe general fitness is not for you, like walking on a treadmill for an hour a few times a week, and that is okey.
You need to find sport that you can go for 100%. You might want to try out a new sport before you can decide what you like most.
Just like you don't want to have a job you hate, you don't want to do a sport you hate for the rest of your life.

> Excersice should be fun, not a chore.

## But at least give it a try, don't be afried you are not good enough

Everyone starts weak af