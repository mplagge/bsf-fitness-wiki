# BSF' Gym starting routine, Full body

Hi :)

## Intensity

As a beginner, the most important thing is that you don't get injured by going all out on day one. Take things slow and you will need to add more Intensity eatch session. Make sure your reps and weight are going up.

Note that this workout can get instense very fast if you push hard on the sets. After a while you should be pushing every excercise as hard as possible.

## What do sets and reps mean (+how much rest in between)?

* Reps is short for repetitions, or the number of times in a row without pause.
* Sets in a workout are how many times you will repeat a the given rep number of a given exercise.

Your rest between sets should be 90 seconds to two minutes. Set a timer to make sure you don't go too early or too late. If you still feel exhausted after two minutes you could take more rest, but it should not be necessary, just keep going. 

Keep your rep ranges from 6-12. If you can do more than 12 reps. Increase the weight a bit. If your form starts to suffer you may need to keep working on your current weight or less. No shame in that.

# The training program

| Training   | M | T | W | T | F | S | S |
|---         |---|---|---|---|---|---|---|
|Bodybuilding|A  |   |B  |   |C  |   |   |
|Cardio      |X  |X  |X  |X  |X  |X  |X  |

## Cardio

Chose one cardio session you might like. Eatch of these rows represents a single cardio session.
You can also use a cross trainer, rowing machine, etc. Medium intensity cardio should be around 20 minutes per session.

|Cardio session|Time (minutes)|
|---|---|
|Bike|20|
|Running|20|
|Walking (maybe walk the dog?)|40|

## Excercises explained

The workout just has 6 excercises to keep things simple. They are the same for every session just in a different order. This is because you can push the hardest on the first few excersices you do. This way you will his every muscle group the hardest once per week.

### Chin up

You may also find that chin ups will be very difficult. Don't worry watch [How To Do Your First Pullup! (Then 8 more!)](https://www.youtube.com/watch?v=mRznU6pzez0) to see how to get started on chin ups/pull ups. Note that the video's uses different rep and set ranges. But the excersices themselfs should be a good progression to 6 solid chin ups.

Just strugle any of the progressions, combinations of them, whatever, untill you have at least a good 8 reps. Be sure to make it more difficult each week.

If you have a cable pulldown machine in your gym this might be a good substitution if you can't do 6 chin ups/pullups.

### TODO all the other stuff.

## A, Push -> Pull --> legs

| Lift | Reps | Sets |
|------|------|------|
|Bench press |6-12|3|
|Militairy press|6-12|3|
|lateral raise|8-15|3|
|BB or dumbbell row|6-12|3|
|Chin up or latpulldown|8-15|3|
|BB Squats|6-12|3|
|Seated hamstring curl|8-15|3|

## B, Pull--> Legs--> Push
| Lift |  Reps | Sets |
|------|--------|------|
|BB row or dumbbell row|6-12|3|
|Chin up or latpulldown|8-15|3|
|BB Squats|6-12|3|
|Seated hamstring curls|8-12|3|
|Bench press|6-12|3|
|Incline dumbbell press|6-12|3|

## C, Legs --> Push --> Pull

| Lift |  Reps | Sets |
|------|--------|------|
|BB Squats|6-12|3|
|BB RDL|6-12|3|
|Bench press |6-12|3|
|Militairy press|6-12|3|
|Lateral raise|8-15|3|
|Chin up/latpulldown|8-15|3|
|BB Row or dummbell row|6-12|3|