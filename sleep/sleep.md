# Sleep Basics

Sleep is essential for recovery. You should be sleeping 8-9 hours, more so if you're younger (going through puberty). When you're sleeping well you will be building muscle and recovering from any sport that you're doing more efficiently.

Sleep hygiene is also important so try to minimise disturbance from light, noise, and invest in a comfy mattress. Reducing the amount of light you're exposed to before bed will help sleep quality, and not eating.

Apps such as sleep cycle can help you with tracking your sleep, it has an alarm and will show you when you're awake, in deep sleep, and sleeping.

Sleep at consistant times everyday. Variable sleep times will significantly reduce your sleep quality. If you have trouble with this you might want to set an alarm to go to bed in the evening, and an alarm in the morning. It should take you a few days to reset your circadian rhythm.
